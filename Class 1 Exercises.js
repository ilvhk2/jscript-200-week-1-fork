// 1. Pagliacci charges $16.99 for a 13” pizza and $19.99 for a 17” pizza. What is the area for each of these pizzas?
// (radius would be the listed size - i.e. 13" - divided by 2)
// 2. What is the cost per square inch of each pizza?
console.log('\n*****  MATH ****');
let diameter=13;
let cost=16.99;
let area = Math.PI * (diameter /2)**2;
let avg = cost/area;
// console.log(`Surface area for ${diameter}\" pizza is ${area.toFixed()}\"`);
// console.log('Cost per square inch of a ' + diameter + '\" pizza is $' + avg.toFixed(2));
const p13=`Surface area for a ${diameter}\" pizza is ${area.toFixed()}\"\n and cost per square inch is $ ${avg.toFixed(2)}`;
console.log(p13);

diameter=17;
cost=19.99;
area = Math.PI * (diameter /2)**2;
avg = cost/area;
//console.log(`\nSurface area for ${diameter}\" pizza is ${area.toFixed()}\"`);
//console.log('Cost per square inch of a ' + diameter + '\" pizza is $' + avg.toFixed(2));
const p17=`Surface area for a ${diameter}\" pizza is ${area.toFixed()}\"\n and cost per square inch is $ ${avg.toFixed(2)}`;
console.log(p17);

// 3. Using the Math object, put together a code snippet that allows you to draw a random card with a value between 1 and 13 (assume ace is 1, jack is 11…)
// 4. Draw 3 cards and use Math to determine the highest card
let max=Math.max(Math.ceil(Math.random()*13),Math.ceil(Math.random()*13),Math.ceil(Math.random()*13));
console.log('\nMethod1: highest random card is ' + max);

//or
const nArray=[Math.ceil(Math.random()*13),Math.ceil(Math.random()*13),Math.ceil(Math.random()*13)];
max = Math.max(...nArray);
console.log('Method2: highest random card is ' + max);

//or
let cards = [];
for (let x=0; x <3; x++){
    cards.push(Math.ceil(Math.random()*13));
}
max = Math.max(...cards);
console.log('Method3: highest random card is ' + max);

let maxCard = 'Highest randaom card of ('+ cards + ') is ' + max ;

// 1. Create variables for firstName, lastName, streetAddress, city, state, and zipCode. Use this information to create a formatted address block that could be printed onto an envelope.
// 2. You are given a string in this format: firstName lastName(assume no spaces in either) streetAddress city, state zip(could be spaces in city and state)
// Write code that is able to extract the first name from this string into a variable. Hint: use indexOf, slice, and / or substring
console.log('\n******  ADDRESS LINE *****');
let firstname='john';
let lastname='smith';
let streetAddress='1234 hello st.';
let city='seattle';
let state='wa';
let zip='98100';
// method 1
let addressBlock =  `${firstname}  ${lastname} 
${streetAddress} 
${city} ${state} ${zip}`;
//or method 2
addressBlock  =  `${firstname}  ${lastname} \n${streetAddress} \n${city} ${state} ${zip}`;
console.log ( addressBlock );

const fs = addressBlock.indexOf(' ');
const fname = addressBlock.substring(0,fs);
console.log ('Extracted the first name is ' + fname);

// On your opwn find the middle date(and time) between the following two dates: 1/1/2020 00:00:00 and 4/1/2020 00:00:00
// Look online for documentation on Date objects.
console.log('\n*****  FIND THE MIDDLE DATE ****');
const startDate = new Date(2020, 0, 1);
const endDate = new Date(2020, 3, 1);
const midDate = new Date((startDate.getTime() + endDate.getTime()) / 2);
// console.log(midDate);
// console.log('MIDDLE DATE is ' + midDate);
console.log(`MIDDLE DATE between ${startDate} and ${endDate} \nis ${midDate}` );
// console.log(midDate.getMonth()+1 + '/' + midDate.getDate() + '/' + midDate.getFullYear() + ' ' + midDate.getHours() +':'+ midDate.getMinutes()+':'+ midDate.getSeconds());
// console.log(`${midDate.getMonth()+1}\/${midDate.getDate()}\/${midDate.getFullYear()} ${midDate.getHours()}\:${ midDate.getMinutes()}\:${ midDate.getSeconds()}`);

/*********  just for fun HTML ************/
document.body.style.backgroundColor = "yellow";
document.body.innerHTML = `<h2>Address</h2>${addressBlock}<br>extracted the first name is ${fname}<h2>Math</h2>${p13}<br>${p17}<br>${maxCard}<br> <h2>DATE</h2>Mid Date is ${midDate}`;

// let x = document.createElement("P");
// let t = document.createTextNode(p13);
// x.appendChild(t);
// document.body.appendChild(x);

// t = document.createTextNode(p17);
// x.appendChild(t);
// document.body.appendChild(x);

